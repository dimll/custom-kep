if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const express = require("express");
const path = require('path');
const ejsMate = require('ejs-mate');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const flash = require('connect-flash');
const morgan = require('morgan');
const passport = require('passport');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');


const app = express();

const User = require('./models/user');

const categoriesRoutes = require('./routes/categories');
const userRoutes = require('./routes/user');
const modRoutes = require('./routes/mod-categories');
const searchRoutes = require('./routes/seach');
const postFilesRoutes = require('./routes/fileHandler');

const dbUrl = process.env.DB_URL;
//const dbLocal = 'mongodb://localhost:27017/kepDemo1';

mongoose.connect(dbUrl,{
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> {
    console.log("Database connected");
});


app.use(express.static(path.join(__dirname, '/public')));


app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

app.use(express.urlencoded({extended: true}));


const store = MongoStore.create({
    mongoUrl: dbUrl,
    touchAfter: 24*3600,
    crypto:{
        secret: process.env.SESSIONCODE
    }
});


store.on('error', function (e) {
    console.log('Session store error ', e);
});

//Change secure after HTTPS dep
const sessionConfig = {
    store,
    name: 'sc',
    secret: process.env.SESSIONCODE,
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        //secure: true,
        expires: Date.now() + 1000*60*60*24*7,
        maxAge: 1000*60*60*24*7
    }
};

app.use(methodOverride('_method'));
app.use(morgan('tiny'));
app.use(flash());
app.use(session(sessionConfig));
app.use(mongoSanitize());
app.use(helmet(    {
    contentSecurityPolicy: false,
    noSniff: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req,res,next) =>{
    res.locals.currentUser = req.user;
    res.locals.success = req.flash('success');
    res.locals.error = req.flash('error');
    next();
});

app.use('/', categoriesRoutes);
app.use('/admin/', modRoutes);
app.use('/user/', userRoutes);
app.use('/search/', searchRoutes);
app.use('/files/', postFilesRoutes);


app.get('*', (req,res,next) => {
    res.render('not-found', {title: '404'})
});

app.use((err,req,res,next) =>{
    res.render('not-found', {title: '404'})
});

//const port = 3000;
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
