
tinymce.init({
    selector: '#content',
    plugins: ' image imagetools media autolink link lists advlist emoticons fullscreen hr',
    toolbar: 'undo redo | bold italic underline | link image media pdfFile XRfile ZIPfile | alignleft aligncenter alignright alignjustify | numlist bullist | emoticons | fullscreen',
    file_picker_types: 'image',
    automatic_uploads: true,
    image_dimensions: true,
    image_description: false,
    height: 250,
    max_height: 400,

    setup: function (editor) {

        editor.ui.registry.addButton('pdfFile', {
            text: 'PDF',
            onAction: function () {
                $('#pdfModal').modal('show');
            }
        });
        editor.ui.registry.addButton('XRfile', {
            text: 'XR tutorial',
            onAction: function () {
                $('#jsonModal').modal('show');
            }
        });
        editor.ui.registry.addButton('ZIPfile', {
            text: 'ZIP file',
            onAction: function () {
                $('#zipModal').modal('show');
            }
        });
        editor.on('init', function(args) {
            editor = args.target;
            $('#topicFormSubmitBtn').prop('disabled', true);
            editor.on('NodeChange', function(e) {
                if (e && e.element.nodeName.toLowerCase() == 'img') {
                    width = e.element.width;
                    height = e.element.height;
                    if (width > 500) {
                        height = height / (width / 500);
                        width = 500;
                    }
                    tinyMCE.DOM.setAttribs(e.element, {'width': width, 'height': height});
                }
            });
        });
        editor.on('input focus click keypress paste mouseover', function () {
            let content = editor.getContent({format: 'text'});
            if ($.trim(content)=='')
            {
                $('#topicFormSubmitBtn').prop('disabled', true);
            }
            else{
                $('#topicFormSubmitBtn').prop('disabled', false);
            }
        });
        editor.on('KeyDown', function (e) {



            if ((e.keyCode == 8 || e.keyCode == 46) && editor.selection) { // delete & backspace keys

                var selectedNode = editor.selection.getNode(); // get the selected node (element) in the editor

                if (selectedNode && selectedNode.nodeName == 'IMG') {

                    console.log("Clicked Delete!");
                    const imgSrc = selectedNode.src;
                    const imgId = imgSrc.substring(imgSrc.lastIndexOf('/')+1);


                    $.ajax({
                        url: '/files/image/'+imgId,
                        type: 'DELETE',
                        data: {},
                        cache: false,
                        contentType: false,
                        processData: false

                    })
                }
            }
        });

    },


    images_upload_url:  '/files/image/upload/'


});

// Prevent Bootstrap dialog from blocking focusin
$(document).on('focusin', function(e) {
    if ($(e.target).closest(".tox-tinymce, .tox-tinymce-aux, .moxman-window, .tam-assetmanager-root").length) {
        e.stopImmediatePropagation();
    }
});

$('#titleInput').on('input', function () {
    let title = $(this).val();
    if (title.length >= 55)
    {
        $('#titleOutOfRange').removeClass('d-none');
    }
    else{
        $('#titleOutOfRange').addClass('d-none');
    }
});

$('#pdfPath').on('change', function () {
    let fileName = $(this).val();
    if (this.files[0].size > 50000000)
    {
        $('#pdfDes').removeClass('text-muted');
        $('#pdfDes').addClass('text-danger');
        $('#uploadPdfBtn').prop('disabled', true);

    }
    else{
        $('#pdfDes').removeClass('text-danger');
        $('#pdfDes').addClass('text-muted');
        $('#uploadPdfBtn').prop('disabled', false);
        fileName = fileName.replace("C:\\fakepath\\", "");
        $(this).next('.custom-file-label').html(fileName);
    }
});

$('#jsonPath').on('change', function () {
    let fileName = $(this).val();
    if (this.files[0].size > 10000000)
    {
        $('#jsonDes').removeClass('text-muted');
        $('#jsonDes').addClass('text-danger');
        $('#uploadJsonBtn').prop('disabled', true);

    }
    else{
        $('#jsonDes').removeClass('text-danger');
        $('#jsonDes').addClass('text-muted');
        $('#uploadJsonBtn').prop('disabled', false);
        fileName = fileName.replace("C:\\fakepath\\", "");
        $(this).next('.custom-file-label').html(fileName);
    }

});

$('#zipPath').on('change', function () {
    let fileName = $(this).val();
    if (this.files[0].size > 200000000)
    {
        $('#zipDes').removeClass('text-muted');
        $('#zipDes').addClass('text-danger');
        $('#uploadZipBtn').prop('disabled', true);

    }
    else{
        $('#zipDes').removeClass('text-danger');
        $('#zipDes').addClass('text-muted');
        $('#uploadZipBtn').prop('disabled', false);
        fileName = fileName.replace("C:\\fakepath\\", "");
        $(this).next('.custom-file-label').html(fileName);
    }

});

$('#closeXBtnPost, #postFormClose').on('click', function () {

    const imgArray = tinymce.activeEditor.selection.dom.select('img');
    const linksArray = tinymce.activeEditor.selection.dom.select('a');

    for (let i=0;i<imgArray.length;i++)
    {
        const imgSrc = imgArray[i].src;
        const imgId = imgSrc.substring(imgSrc.lastIndexOf('/')+1);
        $.ajax({
            url: '/files/image/'+imgId,
            type: 'DELETE',
            data: {},
            cache: false,
            contentType: false,
            processData: false

        })
    }

    for (let i=0;i<linksArray.length;i++){
        let fileStr = String(linksArray[i].href);
        let fileURL = fileStr.split('/');
        let fileId = fileURL[fileURL.length -1];
        let fileType = fileURL[fileURL.length -2];
        if (fileType==='pdf') {
            $.ajax({
                url: '/files/pdf/'+fileId,
                type: 'DELETE',
                data: {},
                cache: false,
                contentType: false,
                processData: false

            })
        }
        else if (fileType==='json'){
            $.ajax({
                url: '/files/json/'+fileId,
                type: 'DELETE',
                data: {},
                cache: false,
                contentType: false,
                processData: false

            })
        }
    }
});


$('#pdfUploadForm').submit(async function (e) {
    e.preventDefault();
    const pdfName = $('#pdfFileName').html();
    const formData = new FormData(this);

    let response = await fetch('/files/pdf/upload/',{
        method: 'POST',
        body: formData
    });

    let pdfId = await response.text();

    $('#pdfModal').modal('hide');
    const pdfLink = `<a href="/files/pdf/${pdfId}">${pdfName}</a>`;
    tinymce.activeEditor.insertContent(pdfLink);


});


$('#jsonUploadForm').submit(async function (e) {
    e.preventDefault();
    const jsonName = $('#jsonFileName').html();
    const formData = new FormData(this);

    let response = await fetch('/files/json/upload/',{
        method: 'POST',
        body: formData
    });

    let jsonID = await response.text();

    $('#jsonModal').modal('hide');
    const jsonLink = `<a href="/files/json/${jsonID}">${jsonName}</a>`;
    tinymce.activeEditor.insertContent(jsonLink);


});

$('#zipUploadForm').submit(async function (e) {
    e.preventDefault();
    const zipName = $('#zipFileName').html();
    const formData = new FormData(this);

    let response = await fetch('/files/zip/upload/',{
        method: 'POST',
        body: formData
    });

    let zipID = await response.text();

    $('#zipModal').modal('hide');
    const zipLink = `<a href="/files/zip/${zipID}" download type="application/zip">${zipName}</a>`;
    tinymce.activeEditor.insertContent(zipLink);

});



const delTopicBtn = document.querySelectorAll('.deleteTopicBtn');
for (let i = 0; i < delTopicBtn.length; i++) {
    delTopicBtn[i].addEventListener('click', function () {
        document.querySelector('.modal-body.deleteTopicModal').innerHTML = delTopicBtn[i].getAttribute('name');
        const delPath = document.querySelector('#deleteTopicForm').action + delTopicBtn[i].id + '?_method=DELETE';
        document.querySelector('#deleteTopicForm').action = delPath;
    })
}






