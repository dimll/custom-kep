
$(window).scroll(async function () {
    if ($(window).scrollTop() >= $(document).height() - $(window).height()) {

        let skip = $('.topic').length;
        //let skip=15;
        const req = await fetch(`/categories/popular/append?skip=${skip}`);
        const newTopics = await req.json();
        if (!jQuery.isEmptyObject(newTopics)){
           for (topic of newTopics)
           {
               let date =  new Intl.DateTimeFormat('en-GB', { dateStyle: 'full'}).format(Date.parse(topic.date));

               let htmlStr = `<div class="row ml-3 mb-3 topic">
                    <div class="d-none d-sm-inline col-sm-2 col-lg-1">
                        <a href="/user/profile/${topic.author._id}">
                            <img class="topic-img" src="/user/${topic.author._id}/profilePic" alt="topic image">
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-9">
                        <div class="row">
                            <div class="col-12"><a href="/categories/${topic.category}/${topic._id}">${topic.title}</a>
                            </div>
                            <div class="col-12 topicDes text-muted">
                                    <span class="date">
                                        ${date}
                                    </span>
                                <span>&nbsp•&nbsp</span>
                                <span class="username"><a href="/user/profile/${topic.author._id}">
                                            ${topic.author.username}</a>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="d-none d-sm-inline col-sm-2 col-lg-1 text-center">
                        <div class="row">
                            <div class="col-12">${topic.posts.length}</div>
                            <div class="col-12 postNum">POSTS</div>
                        </div>
                    </div>
                    <div class="d-none d-sm-inline col-sm-2 col-lg-1 text-center">
                        <div class="row">
                            <div class="col-12">${topic.views}</div>
                            <div class="col-12 viewsNum">VIEWS</div>
                        </div>
                    </div>
                </div>`;

               let newContent = jQuery.parseHTML(htmlStr);
               $('#topics-container').append(newContent);
           }
        }

    }
});
