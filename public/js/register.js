
document.querySelector('#pwd').addEventListener('input', () =>{

   const passInput = document.querySelector('#pwd');
   const pwdVal = passInput.value;

   if (pwdVal.length>=6){
      passInput.classList.remove('is-invalid');
      passInput.classList.add('is-valid');
   }
   else{
      passInput.classList.remove('is-valid');
      passInput.classList.add('is-invalid');
   }
   document.querySelector('#confpass').dispatchEvent(new Event('input', {bubbles:true}));

});


document.querySelector('#confpass').addEventListener('input', () =>{

   const pwdVal = document.querySelector('#pwd').value;
   const confInput =  document.querySelector('#confpass');
   const confVal = confInput.value;

   if (confVal===pwdVal && pwdVal.length>=6) {
      document.querySelector('#regBtn').removeAttribute("disabled");
      confInput.classList.remove('is-invalid');
      confInput.classList.add('is-valid')
   }
   else{
      document.querySelector('#regBtn').setAttribute("disabled", "");
      confInput.classList.remove('is-valid');
      confInput.classList.add('is-invalid');
   }
});

document.querySelector('#registrationForm').addEventListener('submit', function (event) {

   if (document.querySelector('#registrationForm').checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
   }
   document.querySelector('#registrationForm').classList.add('was-validated');
});
