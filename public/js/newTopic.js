
tinymce.init({
    selector: '#mytextarea',
    plugins: 'quickbars image media autolink autoresize link lists advlist emoticons',
    toolbar: 'undo redo | bold italic underline | link image media quickimage | alignleft aligncenter alignright alignjustify | numlist bullist | emoticons',
    file_picker_types: 'image',
});


document.querySelector('#newTopicBtn').addEventListener('click', function () {
    document.querySelector('#newTopic').classList.toggle('d-none');
    document.querySelector('#newTopicBtn').classList.toggle('d-none');
});

document.querySelector('#discardTopic').addEventListener('click', function () {
    document.querySelector('#newTopicForm').classList.toggle('d-none');
    document.querySelector('#newTopicBtn').classList.toggle('d-none');
});
