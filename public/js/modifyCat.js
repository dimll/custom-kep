

document.querySelector('#newCatBtn').addEventListener('click', function () {
    document.querySelector('#newCatForm').classList.toggle('d-none')
    document.querySelector('#newCatBtn').classList.toggle('d-none')
});

document.querySelector('#cancelBtn').addEventListener('click', function () {
    document.querySelector('#newCatForm').classList.toggle('d-none')
    document.querySelector('#newCatBtn').classList.toggle('d-none')
});

const delCatBtns = document.querySelectorAll('.deleteCat');

for (let i=0 ; i<delCatBtns.length; i++){
    delCatBtns[i].addEventListener('click', function () {
        document.querySelector('.modal-body').innerHTML = delCatBtns[i].getAttribute('name');
        let delPath = "/admin/categories/" + delCatBtns[i].id +'?_method=DELETE';
        document.querySelector('#deleteCatForm').action = delPath;
    })
}
