
$(window).scroll(async function () {
    if ($(window).scrollTop() >= $(document).height() - $(window).height()) {

        let skip = $('.user').length;
        const req = await fetch(`/user/all-users/append?skip=${skip}`);
        const newUsers = await req.json();
        if (!jQuery.isEmptyObject(newUsers)){
            for (user of newUsers)
            {

                let htmlStr = `<div class="col-2 text-center user mb-4">
                <a href="/user/profile/${user._id}">
                    <img class="user-img" src="/user/${user._id}/profilePic" alt="user image">
                </a>
                <div class="mt-2">
                    <a href="/user/profile/${user._id}">${user.username}</a>
                </div>
            </div>`;

               let newContent = jQuery.parseHTML(htmlStr);
               $('.user-row').append(newContent);
            }
        }

    }
});
