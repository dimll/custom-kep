
const Topic = require('../models/topic');
const Post = require('../models/post');
const User = require('../models/user');

module.exports.isLoggedIn = (req,res,next) =>{
    if (!req.isAuthenticated()){
        req.session.returnTo = req.originalUrl;
        //req.flash('error', 'You must be signed in to post!');
        return res.redirect('/user/login');
    }
    next();
};

module.exports.isTopicAuthor = async (req, res, next) =>{
    const {topicId} = req.params;
    const topic = await Topic.findById(topicId);
    if (!topic.author.equals(req.user._id))
    {return res.redirect('/');} //permission denied
    next();

};

module.exports.isPostAuthor = async (req, res, next) =>{
    const {postId} = req.params;
    const post = await Post.findById(postId);
    if (!post.author.equals(req.user._id))
    {return res.redirect('/');}
    next();
};

module.exports.isUser = async (req, res, next) =>{
    const {userId} = req.params;
    const user = await User.findById(userId);
    if (!user._id.equals(req.user._id))
    {
        return res.redirect('/');
    }
    next();
};
