const Joi = require('joi');

const validateCategory = (req, res, next) =>{

    const categorySchema = Joi.object({
        category: Joi.object({
            title: Joi.string().required(),
            description: Joi.string().allow(''),
            icon: Joi.string().allow('')
        }).required()
    });

    const {error} = categorySchema.validate(req.body);
    if (error){
        console.log("Error validating!!");
        throw new Error();
    }
    else{
        next();
    }
};

module.exports = validateCategory;


