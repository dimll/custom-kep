module.exports.adminLoggedIn = (req,res,next) =>{
    console.log(req.user);
    if (!req.isAuthenticated() || req.user.role!=='admin'){
        req.session.returnTo = req.originalUrl;
        //req.flash('error', 'You must be signed in to post!');
        return res.redirect('/');
    }
    next();
};
