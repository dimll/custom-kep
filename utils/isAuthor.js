module.exports.isAuthor = async (req, res, next) =>{
    const {topicId} = req.params;
    const topic = await Topic.findById(topicId);
    if (!topic.author.equals(req.user._id))
        {return res.redirect('/');} //permission denied
    next();

};
