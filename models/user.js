const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    username:{
        type: String,
        required: true,
        unique: true
    },
    profilePic: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        enum: ['admin','mod','user'],
        default: 'user'
    },
    memberSince:{
        type: Date,
        default: Date.now
    },
    postsNum:{
        type: Number,
        default: 0
    },
    resetPasswordToken:{
        type: String
    },
    resetPasswordExpires:{
        type: Date
    }

});

//userSchema.plugin(passportLocalMongoose);
userSchema.plugin(passportLocalMongoose, {usernameField: 'email'});


module.exports = mongoose.model('User', userSchema);
