const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const topicSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    category:{
        type: Schema.Types.ObjectId
    },
    posts: [{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }],
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    views:{
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Topic', topicSchema);
