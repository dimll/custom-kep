const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    icon: {
        type: String,
        default: "far fa-file"
    },
    topics: [{
        type: Schema.Types.ObjectId,
        ref: 'Topic'
    }],
    postsNum:{
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Category', categorySchema);
