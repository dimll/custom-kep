const express = require('express');
const router = express.Router();

const Category = require('../models/category');
const Topic = require('../models/topic');

const catchAsync = require('../utils/catchAsync');
const validateCategory = require('../utils/val_Category');
const {isLoggedIn} = require('../utils/isLoggedIn');
const {adminLoggedIn} = require('../utils/adminLoggedIn');


router.get('/', (req,res) => {
    res.redirect('/categories');
});

router.get('/categories', adminLoggedIn, catchAsync(async (req,res) => {
    const categories = await Category.find({});
    res.render('mod-home', {categories, title: "Home"});
}));

//Delete also all the inside data!
router.delete('/categories/:id', adminLoggedIn, catchAsync( async (req,res) =>{
    const {id} = req.params;
    await Category.findByIdAndDelete(id);
    res.redirect('/admin/categories');
}));

router.post('/categories/new', adminLoggedIn, catchAsync( async (req, res) =>{

    const category = new Category(req.body.category);
    await category.save();
    req.flash('success', 'Successfully added a new category!');
    res.redirect('/admin/categories');
}));

router.get('/categories/popular', (req,res) =>{
    res.send('Popular categories page');
});


router.get('/categories/recent', (req,res) =>{
    res.send('Recent categories page');
});

router.get('/categories/:id', catchAsync(async (req,res) =>{
    const { id } = req.params;
    const category = await Category.findById(id).populate('topics');
    //console.log(category);
    res.render('topics', {topics: category.topics ,categoryId: id, title: category.title});
}));

router.get('/logout', catchAsync( async (req,res) =>{
    req.logout();
    req.flash('success', 'You have successfully logged out.');
    res.redirect('/');
}));


module.exports = router;
