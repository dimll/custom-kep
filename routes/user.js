require('../app.js');
const express = require('express');
const passport = require('passport');
const User = require('../models/user');

const router = express.Router();

const catchAsync = require('../utils/catchAsync');
const {isLoggedIn, isUser} = require('../utils/isLoggedIn');

const user = require('../controllers/user');

const logAuthOptions = {failureFlash: "Invalid email or password.", failureRedirect: '/user/login'};

router.get('/:userId/profilePic', catchAsync(user.displayImg));

router.get('/all-users', user.displayAllUsers);
router.get('/all-users/append', user.appendUsers);

router.route('/login')
    .get(catchAsync(user.displayLogin))
    .post(passport.authenticate('local', logAuthOptions), catchAsync(user.login));

router.post('/api/login',passport.authenticate('local', logAuthOptions), catchAsync(user.APIlogin));

router.route('/register')
    .get(catchAsync(user.displayRegister))
    .post(catchAsync(user.registerUser));


router.get('/logout', catchAsync(user.logout));

router.get('/profile', catchAsync(user.displayUserProfile));

router.route('/changePassword')
    .get(isLoggedIn, user.displayPassChange)
    .put(isLoggedIn, user.passChange);

router.route('/resetPassword')
    .get(catchAsync(user.displayReset))
    .put(catchAsync(user.sendPasswordReset));

router.route('/reset/:token')
    .get(catchAsync(user.resetPassword))
    .put(catchAsync(user.resetPasswordDone));



router.post('/profile/imageUpload', isLoggedIn, user.uploadImage);

router.get('/profile/:id', catchAsync(user.displayProfile));

router.delete('/:userId',isLoggedIn, isUser, catchAsync(user.deleteUser));


module.exports = router;
