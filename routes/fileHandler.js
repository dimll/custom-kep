const express = require('express');

const catchAsync = require('../utils/catchAsync');
const {isLoggedIn, isTopicAuthor, isPostAuthor} = require('../utils/isLoggedIn');

const postImg = require('../controllers/postImg');
const postPdf = require('../controllers/postPdf');
const postJson = require('../controllers/postJson');
const postZip = require('../controllers/postZip');

const router = express.Router();

//POST CREATION FILES
router.post('/image/upload/', postImg.uploadTopicImg);
router.post('/pdf/upload', postPdf.uploadTopicPdf);
router.post('/json/upload/', postJson.uploadTopicJson);

//IMAGE
router.get('/images/:imgId', postImg.display);

router.post('/image/upload/:topicId', postImg.upload);

router.delete('/image/:imgId', postImg.delete);

//PDF
router.post('/pdf/upload/:topicId', postPdf.upload);

router.get('/pdf/:pdfId', postPdf.display);

router.delete('pdf/:pdfId');

//XR tutorial JSON
router.post('/json/upload/:topicId',postJson.upload);

router.get('/json/all', postJson.getAllIds);

router.get('/json/:jsonId', postJson.display);

router.delete('/json/:jsonId', postJson.delete);

//ZIP files
router.post('/zip/upload/:topicId',postZip.upload);

router.get('/zip/:zipId', postZip.display);

router.delete('/zip/:zipId', postZip.delete);

module.exports = router;
