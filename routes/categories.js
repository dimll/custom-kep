const express = require('express');

const Category = require('../models/category');
const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');

const catchAsync = require('../utils/catchAsync');
const {isLoggedIn, isTopicAuthor, isPostAuthor} = require('../utils/isLoggedIn');

const categories = require('../controllers/categories');

const router = express.Router();


router.get('/', (req,res) => {
    delete req.session.returnTo;
    res.redirect('categories')
});

router.get('/categories', catchAsync(categories.index));

router.get('/categories/files/:type/:id', (req, res) =>{
    const {type, id} = req.params;
    res.redirect(`/files/${type}/${id}`);

});

router.get('/categories/popular', catchAsync(categories.displayPopular));

router.get('/categories/popular/append', catchAsync(categories.appendPopular));

router.get('/categories/recent', catchAsync(categories.displayRecent));

router.get('/categories/recent/append', catchAsync(categories.appendRecent));


router.get('/categories/:id', catchAsync(categories.displayCategory));


router.post('/categories/:id/new-topic',isLoggedIn, catchAsync(categories.createTopic));

router.route('/categories/:id/:topicId')
    .get(catchAsync(categories.displayPosts))
    .post(isLoggedIn, catchAsync(categories.createPost))
    .delete(isLoggedIn, isTopicAuthor, catchAsync(categories.deleteTopic));

router.route('/categories/:id/:topicId/:postId')
    .delete(isLoggedIn, isPostAuthor, catchAsync(categories.deletePost))
    .put(isLoggedIn, isPostAuthor, catchAsync(categories.editPost));

//Like post
router.put('/categories/:id/:topicId/:postId/like',isLoggedIn, catchAsync( categories.likePost));

//API
router.route('/api/:userName/:categoryId/:topicId')
    .post(categories.UnityJSONPost);



module.exports = router;
