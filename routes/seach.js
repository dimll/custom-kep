const express = require('express');
const router = express.Router();



const search = require('../controllers/search');


router.get('/', search.displaySearchPage);


router.post('/searchTerm', search.searchTerm);
router.post('/question', search.question);


module.exports = router;
