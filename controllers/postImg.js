require('../app.js');
const mongoose = require('mongoose');
const path = require('path');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');

const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');

const db = mongoose.connection;

let imageGFS;
db.once('open', function () {
    imageGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postImages'
        });
});

module.exports.uploadTopicImg = (req, res) =>{
    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                bucketName: 'postImages',
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 20000000
        }
    }).single('file');

    upload(req, res, function (err) {
        if (err)
        {
            //console.log("Image size is too big");
            const rep = {'location': 'https://cdn.pixabay.com/photo/2018/01/04/15/51/404-error-3060993_1280.png'};
            return res.json(rep);
        }
        else{
            const imgPath = '../files/images/'+String(req.file.id);
            const rep = {'location': imgPath};
            return res.json(rep);
        }
    });
};

module.exports.upload = (req,res)=>{
    const {topicId} = req.params;
    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                filename: topicId,
                bucketName: 'postImages',
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 20000000
        }
    }).single('file');

    upload(req, res, function (err) {
        if (err)
        {
            //console.log("Image size is too big");
            const rep = {'location': 'https://cdn.pixabay.com/photo/2018/01/04/15/51/404-error-3060993_1280.png'};
            return res.json(rep);
        }
        else{
            const imgPath = '/files/images/'+String(req.file.id);
            const rep = {'location': imgPath};
            return res.json(rep);
        }
    });

};


module.exports.display = (req, res)=>{

    const {imgId} = req.params;
    const imageId = mongoose.Types.ObjectId(imgId);

    imageGFS.find({
        _id: imageId
    })
        .toArray((err,files)=>{
            if (!files || files.length===0) return res.status(500).json({err: "Error finding image"});
            else {
                return imageGFS.openDownloadStream(imageId).pipe(res);
            }
        });
};

module.exports.delete = (req, res) =>{
    const {imgId} = req.params;
    const iId = mongoose.Types.ObjectId(imgId);
    try{
        imageGFS.delete(iId);
    }
    finally {
        //doNothing
    }
};
