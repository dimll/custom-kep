require('../app.js');
const mongoose = require('mongoose');
const path = require('path');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const axios = require('axios').default;

const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');


const db = mongoose.connection;


let gfs;
db.once('open', function () {
    gfs = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'profilePictures'
        });
});


const storage = new GridFsStorage({
    url: process.env.DB_URL,
    file: (req, file) =>{
        return {
            filename: String(req.user._id) + "_profilePic" ,
            id: req.user._id,
            bucketName: 'profilePictures'
        };
    }
});

const upload = multer({
    storage: storage,
    limits:{
        fileSize: 10000000
    }
}).single('imagePath');

module.exports.displayRegister = async (req,res) =>{
    res.render('register', {title: "Register"})
};

module.exports.displayLogin = async (req,res) => {
    res.render('login', {title: "Login"});
};

module.exports.logout = (req,res) =>{
    req.logout();
    req.flash('success', 'You have successfully logged out.');
    res.redirect('/categories');
};

module.exports.displayAllUsers = async (req, res) =>{

    const users = await User.find({}).limit(50);
    res.render('all-users', {users , title: 'Users'});
};

module.exports.appendUsers = async (req, res) =>{

    const skip = req.query.skip && /^\d+$/.test(req.query.skip) ? Number(req.query.skip) : 0;
    const users = await User.find({}, undefined, {skip}).limit(50);
    res.send(users);

};

module.exports.displayUserProfile = (req,res) =>{
    res.render('my-profile', {title: req.user.username});
};

module.exports.displayProfile = async (req,res) => {
    const {id} = req.params;
    const user = await User.findById(id);
    res.render('user-profile', {user, title: user.username});
};

module.exports.deleteUser = async (req,res) => {
    const {userId} = req.params;
    const user = await User.findById(userId);
    await Post.deleteMany({author: user});
    await Topic.deleteMany({author: user});
    try{
        gfs.delete(req.user._id);
    }
    finally{
        await User.findByIdAndDelete(userId);
        req.flash('error','Successfully deleted user!');
        res.redirect('/categories');
    }
};

module.exports.registerUser = async (req,res) =>{
    try {
        const {email, username, password} = req.body;
        const user = new User({email, username});
        if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
            req.flash('error','Please verify Captcha to continue the registration');
            return res.redirect('/user/register');
        }
        //Google reCaptcha v2
        const captchaSecretKey = process.env.RECAPTCHASECRETKEY;
        const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${captchaSecretKey}&response=${req.body['g-recaptcha-response']}&remoteip=${req.connection.remoteAddress}`;
        const googleRes = await axios.post(verifyUrl,{});
        if (googleRes.data.success !== undefined && !googleRes.data.success){
            req.flash('error','Failed to verify Captcha! Please try again.');
            return res.redirect('/user/register');
        }
        const registeredUser = await User.register(user, password);
        req.login(registeredUser, err =>{
            if (err) return next(err);
            req.flash('success','Welcome to the Knowledge Exchange Platform!');
            res.redirect('/categories');
        });
    }
    catch (e) {
        if (e.code===11000) e.message = "A user with the given username is already registered!";
        else e.message = "This email address is already in use!";
        req.flash('error', e.message);
        res.redirect('/user/register');
    }
};

module.exports.login = async (req,res) =>{
    const redirectUrl = req.session.returnTo || '/';

    if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        req.flash('error','Please verify Captcha to login.');
        return res.redirect('/user/login');
    }
    //Google reCaptcha v2
    const captchaSecretKey = process.env.RECAPTCHASECRETKEY;
    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${captchaSecretKey}&response=${req.body['g-recaptcha-response']}&remoteip=${req.connection.remoteAddress}`;
    const googleRes = await axios.post(verifyUrl,{});
    if (googleRes.data.success !== undefined && !googleRes.data.success){
        req.flash('error','Failed to verify Captcha! Please try again.');
        return res.redirect('/user/login');
    }

    delete req.session.returnTo;
    if (req.user.role==='admin') return res.redirect('/admin/categories');
    req.flash('success','Logged in successfully!');
    res.redirect(redirectUrl);
};

module.exports.APIlogin = (req,res) =>{
    return res.json('Logged in successfully');
};

module.exports.displayPassChange = (req,res) => {
    res.render('pass-change', {title: "Password change"});
};

module.exports.displayReset = (req, res) =>{
    res.render('reset-pass', {title: "Password reset"})
};

//Change mail options
module.exports.sendPasswordReset = async (req, res) =>{
    await User.findOne({email: req.body.email}, async function (err, user) {
        if (!user){
            req.flash('success', 'If the specified address corresponds to an existing user account, a password reset email was sent.');
            console.log('User not found!');
            return res.redirect('/categories');
        }
        else{
            const buffer = crypto.randomBytes(20);
            const token = buffer.toString('hex');
            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000;
            await user.save();

            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: `${process.env.KEPEMAIL}`,
                    pass: `${process.env.KEPEMAILPASS}`,
                }
            });
            const mailOptions = {
                to: String(req.body.email),
                from: '"Knowledge Exchange Platform Robot" <keprobot@gmail.com>',
                subject: 'Knowledge Exchange Platform Password Reset',
                text: `We received a request to reset your password, possibly because you have forgotten it. 
                      If this is not the case, please ignore this email. 
                      To reset your password please click the following link: 
                      http://${req.headers.host}/user/reset/${token}`


            };
            transporter.sendMail(mailOptions, function (err) {
                if (err){
                    console.log("Error sending email");
                }
            });
            req.flash('success', 'If the specified address corresponds to an existing user account, a password reset email was sent.');
            return res.redirect('/categories');
        }
    });

};

module.exports.resetPassword = async (req, res) =>{
    const {token} = req.params;
    await User.findOne({resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()}}, function (err, user) {
        if (!user || err){
            req.flash('error', 'Password reset token is invalid or has expired');
            return res.redirect('/categories');
        }
        res.render('reset-done', {user: user, title: 'Change Password'});
    });
};

module.exports.resetPasswordDone = async (req, res) =>{
    const {token} = req.params;
    await User.findOne({resetPasswordToken: token, resetPasswordExpires: {$gt : Date.now()}}, function (err,user) {
        if (!user || err){
            req.flash('error', 'Password reset token is invalid or has expired');
            return res.redirect('/categories');
        }
        user.setPassword(req.body.password, async function () {
            if (err){
                req.flash('error', 'Failed to change password. Please try again later.');
                return res.redirect('/categories');
            }
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;
            await user.save();
            req.flash('success', 'Your password has been changed! Please log in again.');
            res.redirect('/categories');
        })

    })


};

module.exports.passChange = async (req, res) => {
    const {currentPass, newPass} = req.body;
    User.findOne({ _id: req.user._id },(err, user) => {
        if (err) {
            req.flash('error','Your password is incorrect. Please try again!');
            return res.redirect('/user/changePassword');
        }
        else {
            user.changePassword(currentPass, newPass, function(err) {
                if(err) {
                    req.flash('error','Your password is incorrect. Please try again!');
                    return res.redirect('/user/changePassword');
                } else {
                    req.flash('success','Your password has been changed successfully!');
                    return res.redirect('/user/profile');
                }
            })
        }
    });
};

module.exports.uploadImage = async (req,res) => {
    const user = User.findById(req.user._id);
    if (user.profilePic)
    {
        gfs.delete(req.user._id);
    }
    upload(req, res, async function (err) {
        if (err) {
            req.flash('error','Upload failed. Please choose an image less than 10MB!');
            res.redirect('/user/profile');
        }
        else
        {
            await User.findByIdAndUpdate(req.user._id, {$set: {profilePic: true}});
            req.flash('success','Image uploaded successfully!');
            res.redirect('/user/profile');

        }
    })
};

module.exports.displayImg = async (req,res) => {
    const {userId} = req.params;
    const user = await User.findById(userId);
    if (!user.profilePic) {
        return res.sendFile(path.join(__dirname, '../public', '/images/blank_profile_pic.png'));
    }
    else {
        const fn = String(userId)+'_profilePic';
        gfs.find({
            filename: fn
        })
            .toArray((err,files)=>{
                if (!files || files.length===0) return res.status(500).json({err: "Error finding profile picture"});
                else {
                    return gfs.openDownloadStreamByName(fn).pipe(res);
                }
            });
    }
};
