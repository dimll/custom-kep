require('../app.js');
const mongoose = require('mongoose');
const Joi = require('joi');

const Category = require('../models/category');
const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');


const db = mongoose.connection;

let imageGFS, pdfGFS, jsonGFS;
db.once('open', function () {
    imageGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postImages'
        });
    pdfGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postPdfs'
        });
    jsonGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postJsons'
        });
});

async function filesDrop (topicId){

    let cursor = db.collection('postImages.files').find({filename: topicId});
    await cursor.forEach(img =>{
        imageGFS.delete(img._id);
    });
    cursor = db.collection('postPdfs.files').find({filename: topicId});
    await cursor.forEach(pdf =>{
        pdfGFS.delete(pdf._id);
    });
    cursor = db.collection('postJsons.files').find({filename: topicId});
    await cursor.forEach(jsonFile =>{
        jsonGFS.delete(jsonFile._id);
    });

}

module.exports.index = async (req,res) => {

    const categories = await Category.find({});
    res.render('home', {categories, title: "Home"});
};

module.exports.displayCategory = async (req,res, next) =>{
    const {id} = req.params;
    //const category = await Category.findById(id).populate('topics');
    const category = await Category.findById(id).populate({
        path: 'topics',
        populate: {path: 'author'}
    });
    res.render('topics', {topics: category.topics, categoryId: id, title: category.title});
};


module.exports.displayPopular = async (req, res) =>{

    const topics = await Topic.find({}).sort({views: -1}).limit(10).populate('author');
    res.render('popular', {topics, title: 'Popular Topics'});
};

module.exports.appendPopular = async (req, res) =>{

    const skip = req.query.skip && /^\d+$/.test(req.query.skip) ? Number(req.query.skip) : 0;
    const topics = await Topic.find({}, undefined, {skip}).sort({views: -1}).limit(10).populate('author');

    res.send(topics);
};


module.exports.displayRecent = async (req, res) =>{

    const topics = await Topic.find({}).sort({date: -1}).limit(10).populate('author');

    res.render('recent', {topics, title: 'Recent Topics'});
};

module.exports.appendRecent = async (req, res) =>{

    const skip = req.query.skip && /^\d+$/.test(req.query.skip) ? Number(req.query.skip) : 0;
    const topics = await Topic.find({}, undefined, {skip}).sort({date: -1}).limit(10).populate('author');

    res.send(topics);
};

module.exports.createTopic = async (req,res, next) =>{

    const {id} = req.params;
    const category = await Category.findById(id);
    const topic = new Topic({title: req.body.title});
    const post = new Post({content: req.body.content});

    const author = await User.findById(req.user._id);
    await User.findByIdAndUpdate(req.user._id, {$set: {postsNum: author.postsNum + 1}});
    await Category.findByIdAndUpdate(id, {$set: {postsNum: category.postsNum + 1}});

    topic.category = mongoose.Types.ObjectId(id);
    topic.author = author;
    post.author = author;
    topic.posts.push(post);
    category.topics.push(topic);

    await post.save();
    await topic.save();
    await category.save();

    res.redirect(`/categories/${id}`);
};

module.exports.createPost = async (req,res, next) =>{


    const {id, topicId} = req.params;
    const post = new Post({content: req.body.content});
    const category = await Category.findById(id);
    const topic = await Topic.findById(topicId);
    const author = await User.findById(req.user._id);

    await User.findByIdAndUpdate(req.user._id, {$set: {postsNum: author.postsNum + 1}});
    await Category.findByIdAndUpdate(id, {$set: {postsNum: category.postsNum + 1}});

    post.author = author;
    topic.posts.push(post);

    await post.save();
    await topic.save();

    res.redirect(`/categories/${id}/${topicId}`);
};


module.exports.displayPosts = async (req,res, next) =>{
    const {id, topicId} = req.params;
    req.session.returnTo = req.originalUrl;
    const category = await Category.findById(id);
    const topic = await Topic.findById(topicId).populate({
        path: 'posts',
        populate: {path: 'author'}
    });
    await Topic.findByIdAndUpdate(topicId, {$inc: {views: 1}});
    res.render('posts', {topic, category, title: topic.title});
};


module.exports.deleteTopic = async (req,res) =>{
    const {id, topicId} = req.params;
    const topic = await Topic.findById(topicId).populate({
        path: 'posts',
        populate: {path: 'author'}
    });
    const category = await Category.findById(id);

    //Delete user's post number
    for (let post of topic.posts)
    {
        await User.findByIdAndUpdate(post.author._id, {$inc: {postsNum: - 1}});
    }

    await Category.findByIdAndUpdate(id, {$pull: {topics: topicId},
        $set:{ postsNum: category.postsNum - topic.posts.length}
    });
    await Post.deleteMany({_id: {$in: topic.posts}});
    await Topic.findByIdAndDelete(topicId);


    filesDrop(topicId);



    res.redirect(`/categories/${id}`);
};

module.exports.deletePost = async (req,res) =>{

    const {id, topicId, postId} = req.params;
    const author = await User.findById(req.user._id);
    const category = await Category.findById(id);


    await Topic.findByIdAndUpdate(topicId, {$pull: {posts: postId}});
    await Post.findByIdAndDelete(postId);
    await User.findByIdAndUpdate(req.user._id, {$set: {postsNum: author.postsNum - 1}});
    await Category.findByIdAndUpdate(id, {$set: {postsNum: category.postsNum - 1}});

    res.redirect(`/categories/${id}/${topicId}`);

};

module.exports.editPost = async (req,res) =>{
    const {id, topicId, postId} = req.params;
    const {content} = req.body;
    await Post.findByIdAndUpdate(postId, {$set: {content: content}});
    res.redirect(`/categories/${id}/${topicId}`);

};

module.exports.likePost = async (req,res) =>{
    const {postId} = req.params;
    const post = await Post.findById(postId);
    const idx = post.likes.indexOf(req.user._id);
    if (idx>-1) post.likes.splice(idx, 1);
    else post.likes.push(req.user);
    await Post.findByIdAndUpdate(postId, {likes: post.likes});

};

//API

module.exports.UnityJSONPost = async (req,res)=>{
    const {userName, categoryId, topicId} = req.params;

    console.log("Username: "+userName);
    console.log("Body content: "+req.body.content);

    const post = new Post({content: req.body.content});
    const category = await Category.findById(categoryId);
    const topic = await Topic.findById(topicId);
    const author = await User.findOne({username: userName});
    let status = "failed";
    if (author) {
        await User.findByIdAndUpdate(author._id, {$inc: {postsNum: 1}});
        await Category.findByIdAndUpdate(categoryId, {$set: {postsNum: category.postsNum + 1}});

        post.author = author;
        topic.posts.push(post);

        await post.save();
        await topic.save();
        status = "successful";
    }
    res.send(`Post ${status}`);


};
