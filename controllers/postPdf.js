require('../app.js');
const mongoose = require('mongoose');
const path = require('path');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const axios = require('axios').default;

const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');

const db = mongoose.connection;

let pdfGFS;
db.once('open', function () {
    pdfGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postPdfs'
        });
});


module.exports.uploadTopicPdf = (req,res) =>{

    const newId = mongoose.Types.ObjectId();

    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                id: newId,
                bucketName: 'postPdfs'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 50000000 //50MB pdf limit
        }
    }).single('pdfPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading PDF file! Please try again later.');
        }
        else
        {
            //res.send(req.file);
            //console.log('File Uploaded!');
        }
    });
    res.send(String(newId));

};


module.exports.upload = (req, res)=>{

    const {topicId} = req.params;
    const newId = mongoose.Types.ObjectId();
    //console.log(String(newId));

    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                filename: topicId,
                id: newId,
                bucketName: 'postPdfs'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 50000000 //50MB pdf limit
        }
    }).single('pdfPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading PDF file! Please try again later.');
        }
        else
        {
            //res.send(req.file);
            //console.log('File Uploaded!');
        }
    });
    res.send(String(newId));
};


module.exports.display = (req, res)=>{

    const {pdfId} = req.params;
    pdfOId = mongoose.Types.ObjectId(pdfId);

    pdfGFS.find({
        _id: pdfOId
    })
        .toArray((err,files)=>{
            if (!files || files.length===0) return res.status(500).json({err: "Error finding pdf"});
            else {
                return pdfGFS.openDownloadStream(pdfOId).pipe(res);
            }
        });
};

module.exports.delete = (req,res) =>{
    const {pdfId} = req.params;
    const pId = mongoose.Types.ObjectId(pdfId);
    try{
        pdfGFS.delete(pId);
    }
    finally { }
};
