require('../app.js');
const mongoose = require('mongoose');
const path = require('path');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const axios = require('axios').default;

const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');

const db = mongoose.connection;

let zipGFS;
db.once('open', function () {
    zipGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postZip'
        });
});


module.exports.uploadTopicZip = (req, res) => {

    const newId = mongoose.Types.ObjectId();
    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                id: newId,
                bucketName: 'postZip'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 200000000 //200MB zip limit
        }
    }).single('zipPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading ZIP file! Please try again later');
        }
        else  {}
    });
    res.send(String(newId));
};


module.exports.upload = (req, res)=>{

    const {topicId} = req.params;
    const newId = mongoose.Types.ObjectId();

    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                filename: topicId,
                id: newId,
                bucketName: 'postZip'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 200000000
        }
    }).single('zipPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading zip file! Please try again later');
        }
        else
        {
            //console.log('JSON File Uploaded!');
        }
    });
    res.send(String(newId));
};


module.exports.display = (req, res)=>{

    const {zipId} = req.params;
    zipOId = mongoose.Types.ObjectId(zipId);

    zipGFS.find({
        _id: zipOId
    })
        .toArray((err,files)=>{
            if (!files || files.length===0) return res.status(500).json({err: "Error finding zip file"});
            else {
                return zipGFS.openDownloadStream(zipOId).pipe(res);
            }
        });
};

module.exports.delete = (req,res) =>{
    const {zipId} = req.params;
    zipOId = mongoose.Types.ObjectId(zipId);
    try{
        jsonGFS.delete(zipOId);
    }
    finally {
        //doNothing
    }
};
