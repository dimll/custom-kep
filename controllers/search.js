
const Post = require('../models/post');
const Category = require('../models/category');
const Topic = require('../models/topic');

module.exports.displaySearchPage = (req, res)=>{
    res.render('search', {searchTerm: null, foundCategories: null, foundTopics:null, title: "Search"});
};


module.exports.searchTerm = async (req, res) =>{
    const {searchTerm} = req.body;

    let foundCategories = await Category.find({title: {$regex: searchTerm}});
    if (foundCategories.length===0)
    {
        let searchTerm2 = searchTerm.charAt(0).toUpperCase() + searchTerm.slice(1);
        foundCategories =  await Category.find({title: {$regex: searchTerm2}});
        if (foundCategories.length===0)
        {
            searchTerm2 = searchTerm.charAt(0).toUpperCase() + searchTerm.slice(1).toLowerCase();
            foundCategories = await Category.find({title: {$regex: searchTerm2}});
        }
    }

    let foundTopics = await Topic.find({title: {$regex: searchTerm}});
    if (foundTopics.length===0)
    {
        let searchTerm2 = searchTerm.charAt(0).toUpperCase() + searchTerm.slice(1);
        foundTopics =  await Topic.find({title: {$regex: searchTerm2}});
        if (foundTopics.length===0)
        {
            searchTerm2 = searchTerm.charAt(0).toUpperCase() + searchTerm.slice(1).toLowerCase();
            foundTopics = await Topic.find({title: {$regex: searchTerm2}});
        }
    }


    res.render('searchRes', {searchTerm, foundCategories, foundTopics, title: 'Search Results'});
};

module.exports.question = async (req,res) =>{
    const {question} = req.body;
    res.render('searchAns', {question, title: 'Search Results'});
};
