require('../app.js');
const mongoose = require('mongoose');
const path = require('path');
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const axios = require('axios').default;

const Topic = require('../models/topic');
const User = require('../models/user');
const Post = require('../models/post');

const db = mongoose.connection;

let jsonGFS;
db.once('open', function () {
    jsonGFS = new mongoose.mongo.GridFSBucket(db.db,
        {
            bucketName: 'postJsons'
        });
});


module.exports.uploadTopicJson = (req, res) => {

    const newId = mongoose.Types.ObjectId();
    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                id: newId,
                bucketName: 'postJsons'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 10000000 //10MB JSON limit
        }
    }).single('jsonPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading JSON file! Please try again later');
        }
        else
        {
            //console.log('JSON File Uploaded!');
        }
    });
    res.send(String(newId));
};

module.exports.getAllIds = async (req,res) =>{
    const jsonFiles = await jsonGFS.find({}).toArray();
    const jsonIds = [];
    for (let f of jsonFiles)
    {
        jsonIds.push(f._id);
    }
    return res.json(jsonIds);
};

module.exports.upload = (req, res)=>{

    const {topicId} = req.params;
    const newId = mongoose.Types.ObjectId();

    const storage = new GridFsStorage({
        url: process.env.DB_URL,
        file: (req, file) =>{
            return {
                filename: topicId,
                id: newId,
                bucketName: 'postJsons'
            };
        }
    });

    const upload = multer({
        storage: storage,
        limits:{
            fileSize: 10000000 //10MB JSON limit
        }
    }).single('jsonPath');

    upload(req, res, async function (err) {
        if (err) {
            res.send('Error in uploading JSON file! Please try again later');
        }
        else
        {
            //console.log('JSON File Uploaded!');
        }
    });
    res.send(String(newId));
};


module.exports.display = (req, res)=>{

    const {jsonId} = req.params;
    jsonOId = mongoose.Types.ObjectId(jsonId);

    jsonGFS.find({
        _id: jsonOId
    })
        .toArray((err,files)=>{
            if (!files || files.length===0) return res.status(500).json({err: "Error finding JSON file"});
            else {
                return jsonGFS.openDownloadStream(jsonOId).pipe(res);
            }
        });
};

module.exports.delete = (req,res) =>{
    const {jsonId} = req.params;
    jsonOId = mongoose.Types.ObjectId(jsonId);
    try{
        jsonGFS.delete(jsonOId);
    }
    finally {
        //doNothing
    }
};
